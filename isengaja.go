package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"time"
)

func main() {
	now := time.Now()
	elaspsedsince := time.Since(now).Seconds()
	fmt.Println("pake since: ", elaspsedsince)
	elapsedafter := time.Now().After(now)
	fmt.Println("pake after: ", elapsedafter)
	elapsedsub := time.Now().Sub(now)
	fmt.Println("pake sub: ", elapsedsub)
	ultramanTiga()
	// http.HandleFunc("/", gloriousWebScaleGoFunction)
	// http.ListenAndServe(":2020", nil)
}

func gloriousWebScaleGoFunction(w http.ResponseWriter, r *http.Request) {
	file, err := ioutil.ReadFile("lol-no-generics.html")
	if err != nil {
		os.Exit(-1)
	}
	io.WriteString(w, string(file))
}

// Node something something.
type Node struct {
	Data  int
	Left  *Node
	Right *Node
}

func newNode(data int) *Node {
	return &Node{Data: data, Left: nil, Right: nil}
}

// helpBuildPreorder dari https://www.geeksforgeeks.org/construct-bst-from-given-preorder-traversa/
func helpBuildPreorder(pre []int, preindex int, key int, min int, max int, size int) *Node {
	var root *Node
	if key > min && key < max {
		root = newNode(key)
		if preindex < size {
			root.Left = helpBuildPreorder(pre, preindex+1, pre[preindex+1], min, key, size)
			root.Right = helpBuildPreorder(pre, preindex+1, pre[preindex+1], key, max, size)
		}
	}
	return root
}

func buildPreorder(pre []int, size int) *Node {
	preindex := 0
	return helpBuildPreorder(pre, preindex, pre[0], math.MinInt32, math.MaxInt32, size)
}

// PrintInorder kiri terus sampai habis baru kanan.
func (n *Node) PrintInorder() {
	if n == nil {
		return
	}
	n.Left.PrintInorder()
	fmt.Println(n.Data)
	n.Right.PrintInorder()
}

// GetAnInteger ambil integer dari console.
func GetAnInteger() int {
	var anu int
	_, err := fmt.Scanf("%d", &anu)
	if err != nil {
		panic("Bukan integer.")
	}
	return anu
}

// GetIntList ambil input dari user.
func GetIntList() (int, []int) {
	fmt.Print("Jumlah node: ")
	jumlah := GetAnInteger()
	isian := make([]int, jumlah)
	for i := 0; i < jumlah; i++ {
		fmt.Print("Node ke ", i+1)
		fmt.Print(": ")
		isian[i] = GetAnInteger()
	}
	return jumlah, isian
}

// ultramanTiga nomor tiga.
func ultramanTiga() {
	jumlah, pre := GetIntList()
	n := buildPreorder(pre, jumlah)
	n.PrintInorder()
}
