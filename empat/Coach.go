package main

import (
	"html/template"
	"log"
	"net/http"
)

// lihatForm handler untuk lihat form.
func lihatForm(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("GET only, please."))
		return
	}
	t, _ := template.ParseFiles("sesuatu.gtpl")
	t.Execute(w, nil)
}

// sesuatu handler untuk sesuatu.
func sesuatu(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("POST only, please."))
		return
	}
	r.ParseForm()
	w.Write([]byte(r.Form.Get("date")))
}

func main() {
	http.HandleFunc("/", lihatForm)
	http.HandleFunc("/sesuatu", sesuatu)
	err := http.ListenAndServe(":2020", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
